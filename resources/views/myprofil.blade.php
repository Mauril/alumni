<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href={{asset("gi/single/css/bootstrap-icons.css")}}>
    <link rel="stylesheet" href={{asset("gi/single/css/bootstrap.min.css")}}>
    {{-- <link rel="stylesheet" href={{asset("main.css")}}> --}}


    <title>Portfolio</title>
</head>

<style>
    .btn-primary {
   background-color: #ff695f;
   border-color: #ff695f;

}
.btn-primary:hover{
    background-color: #e05e55;
    border-color: #e05e55;
}
</style>
<body>
    {{-- <header class=" mb-4">
        <nav class="navbar navbar-light bg-light navbar-expand-lg ">
            <div class="container ">
              <a class="navbar-brand col-sm-3" href="#">
                  <h2>Alumni</h2>
                  <a href="" class="nav-link " style="font-size: larger;"> <i class="bi bi-arrow-left"></i> retour </a>

              </a>
             
              
            </div>
        </nav>
    </header> --}}

    <style>
        .navbar ul {
          margin: 0;
          padding: 0;
          display: flex;
          list-style: none;
          align-items: center;
        }
      
        .navbar li {
          position: relative;
        }
      
        .navbar>ul>li {
          white-space: nowrap;
          padding: 10px 0 10px 30px;
        }
      
        .navbar a,
        .navbar a:focus {
          display: flex;
          align-items: center;
          justify-content: space-between;
          color: black;
          font-size: 16px;
          font-weight: 600;
          padding: 0;
          white-space: nowrap;
          transition: 0.3s;
          letter-spacing: 0.4px;
          position: relative;
        }
        .navbar a:hover{
          color: #03a4ed;
        }
      
        .list-group-item{
      
          border: solid rgb(27 172 240 / 26%) 2px !important ;
          padding: 15px;
          margin: 10px;
          border-radius: 10px;
        }
        .active{
            color: #03a4ed !important;
        }
        .content{
          margin-top: 8% !important;
        }
      </style>
    <header class="bg-light fixed-top shadow">
        <div class="container d-flex align-items-center justify-content-between ">
         <h1 class="logo">Alumni</h1>
   
       <nav id="navbar" class="navbar">
         <ul>
          <li><a class="nav-link " href="{{route('profile.index')}}">Liste des alumnis</a></li>
           <li><a class="nav-link " href="{{route('userposts')}}">gerer mes publications</a></li>
           <li><a class="nav-link " href="{{route('posts.index')}}">mur des publications</a></li>
           <li><a class="nav-link active" href="">mon profil</a></li>
           <li>
            <form action="{{route('logout')}}" method="POST">
              @csrf
              <button type="submit" class="btn btn-sm btn-primary">Deconnexion</button>
            </form>
          </li>
          
         </ul>

       </nav>

    
        </div>
     </header>
    <section class="container content" style="background-image: url({{asset("gi/single/img/back3.png")}}); background-repeat: no-repeat; background-size: cover;">
        <div class="row">
            <div class=" mb-4 d-flex justify-content-between">
                <div class="h3 fw-bolder " style="color:#03a4ed"> Mon Profil</div>
               <a href="{{route('profile.modify')}}"><button class="btn btn-info btn-sm mb-2">Editer mon profil</button></a>
            </div>
           
            <div class="col-md-5 d-flex align-items-start">
                <div>
                    <div class="h3 fw-bolder pb-1">{{$item->nom}} {{$item->prenom}}</div>
                    <div class="h4 pb-2">{{$item->competence->nom}}</div>
                    <div class="h4 pb-2"> Filiere :  <span class="badge bg-info"> {{$item->filiere->nom}} </span> </div>
                    <div class="h4 pb-2">Année de fin d'etude : <span class="h5 fw-normal"> {{$item->annee_fin_etude}} </span>  </div>
                    <div class="h4 pb-2">Theme de soutenance  : <span class="h5 fw-normal"> {{$item->theme_soutenance}} </span></div>
                    <div class="h4 pb-2">Age : <span class="h5 fw-normal"> {{$item->age}} ans</span></div>
                    <div class="h4 pb-2">Travail actuel :<span class="h5 fw-normal">
                        @if($item->post_id != null)
                         occupe le poste de {{$item->post->nom}} dans l'entreprise {{$item->entreprise->nom}} 
                        @else
                            Aucun
                        @endif
                        </span></div>
                    <div class="h4 pb-2">Email : <span class="h5 fw-normal"> {{$item->email}} </span></div>
                    <div class="h4 pb-2">Tel : <span class="h5 fw-normal"> {{$item->telephone}} </span></div>
                    <a href="{{asset("storage/$item->cv")}}" target="_blank" type="application/pdf"><button class="btn btn-primary">voir mon CV</button></a>
                </div>
            </div>
            <div class=" offset-md-2 col-md-5">
                <img class="img-fluid w-100 " style="border-radius: 2%;" src="{{asset("storage/$item->profil_image")}}">
            </div>
        </div>
    </section>
 
    <footer style="background-color: rgb(252, 243, 232); font-family: Verdana, Geneva, Tahoma, sans-serif;" class="text-center py-5 mt-5">
        <div class="row">
            
        </div>
    </footer>
    <script src="{{asset("gi/single/bootstrap.bundle.min.js")}}"></script>
</body>
</html>