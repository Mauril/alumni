<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href={{asset("gi/posts/boostrap.min.css")}}>
    <link rel="stylesheet" href={{asset("gi/posts/chat.css")}}>
</head>
<body>
        <style>
            .navbar ul {
              margin: 0;
              padding: 0;
              display: flex;
              list-style: none;
              align-items: center;
            }
          
            .navbar li {
              position: relative;
            }
          
            .navbar>ul>li {
              white-space: nowrap;
              padding: 10px 0 10px 30px;
            }
          
            .navbar a,
            .navbar a:focus {
              display: flex;
              align-items: center;
              justify-content: space-between;
              color: black;
              font-size: 16px;
              font-weight: 600;
              padding: 0;
              white-space: nowrap;
              transition: 0.3s;
              letter-spacing: 0.4px;
              position: relative;
            }
            .navbar a:hover{
              color: #03a4ed;
            }
          
            .list-group-item{
          
              border: solid rgb(27 172 240 / 26%) 2px !important ;
              padding: 15px;
              margin: 10px;
              border-radius: 10px;
            }
            .active{
                color: #03a4ed !important;
            }
            .content{
              margin-top: 5% !important;
            }
          </style>
        <header class="bg-light fixed-top shadow">
            <div class="container d-flex align-items-center justify-content-between ">
             <h1 class="logo">Alumni</h1>
       
           <nav id="navbar" class="navbar">
             <ul>
              <li><a class="nav-link active" href="{{route('profile.index')}}">Liste des alumnis</a></li>
               <li><a class="nav-link " href="{{route('userposts')}}">gerer mes publications</a></li>
               <li><a class="nav-link " href="{{route('posts.index')}}">mur des publications</a></li>
               <li><a class="nav-link" href="{{route('home')}}">mon profil</a></li>
               <li>
                <form action="{{route('logout')}}" method="POST">
                  @csrf
                  <button type="submit" class="btn btn-sm btn-primary">Deconnexion</button>
                </form>
              </li>
              
             </ul>
    
           </nav>
    
        
            </div>
         </header>
        <section class="container content" style="background-image: url({{asset("gi/single/img/back3.png")}}); background-repeat: no-repeat; background-size: cover;">
            <div class="h3" style="color:#03a4ed"> Liste des alumnis</div>
            
                <div class="messages-box">
                    <div class="list-group " >
                      @forelse($items as $item)
                      <div class="list-group-item list-group-item-action mb-3" style="background-image: url({{asset('gi/posts/img/back.png')}}); background-repeat: no-repeat;"  >
                        <div class="media w-100"><img 
                            @if($item->profil_image !=null)
                            src = {{asset("storage/$item->profil_image")}}
                            @else
                            src = {{asset("gi/posts/img/avatar.svg")}}
                            @endif
                            
                            alt="user" width="75" height = "75" style ="border-radius: 50%">
                          <div class="media-body ml-4">
                            <div class="d-flex align-items-center justify-content-between mb-1">
                              <h6 class="m-2"> <a href="{{route('profile.show',['id'=>$item->id])}}">{{$item->nom}} {{$item->prenom}}</a> </h6><small class="small font-weight-bold"> {{$item->filiere->nom}} </small>
                            </div>
                           <p class="m-2">
                            @if($item->competence_id !=null)
                            {{$item->competence->nom}}
                            @endif
                           </p>
                          </div>
                        </div>
                      </div>
                      @empty
                          <div class="h5">
                            Aucun alumni trouvé
                          </div>
                      @endforelse
                    </div>
                  </div>
            
        </section>
     
        <footer style="background-color: rgb(252, 243, 232); font-family: Verdana, Geneva, Tahoma, sans-serif;" class="text-center py-5 mt-5">
            <div class="row">
                
            </div>
        </footer>
        <script src="{{asset("gi/single/bootstrap.bundle.min.js")}}"></script>
</body>
</html>