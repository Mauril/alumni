<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script src="{{ asset('all.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('all.css') }}">
    <title>Inscription</title>
    <style>
        @import url(./template1/css.css);

        * {
            box-sizing: border-box;
        }

        body {

            margin: 0;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-repeat: no-repeat;
            background: url(./template1/assets/images/portfolio-left-dec.png);
        }

        .container {
            position: relative;
            width: 450px;
            min-height: 555px;
            height: auto;
            max-height: 520px;
            background-color: #fffdfc;
            margin-top: 3%;
            padding: 15px 25px;
            border-radius: 10px;
            font-size: 18px;
            font-family: 'Quicksand', sans-serif;
        }



        .tab-body {
            position: relative;
            display: none;
            height: auto;
            max-height: 320px;
            padding: 15% 5%;
            margin-top: 20%;
            background-color: #fff;
            color: #82ade6de;
            box-shadow: 0px 5px 10px #7c8395;
            border-radius: 30px;
            font-size: 1rem;
        }

        .tab-body.active {
            display: block;
        }

        .top {
            color: #8a8a8ade;
        }


        .row {
            display: flex;
            justify-content: space-between;
            border-bottom: 1px solid #8a8a8ade;
            padding: 1.8% 0;
            margin-top: 20px;
        }

        .row .input {
            width: 90%;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        label {
            font-size: 0.9rem;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-size: 13px;
            margin-left: 20px;
        }

        span {
            font-size: 0.5rem;
            border: none;
            outline: none;
            background-color: transparent;
            color: #f84747de;
            font-weight: bold;
            margin-top: auto;

        }

        .tl {
            font-family: "Quicksand", sans-serif;
        }


        .row .date {
            width: 90%;
            border: none;
            outline: none;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        .row select {
            width: 90%;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        .input::placeholder {
            color: #8a8a8ade;
            font-family: 'Quicksand', sans-serif;
        }



        .select {
            margin-left: 7%;
        }

        .tel {
            color: #8a8a8ade;
        }

        .icon {
            color: #8a8a8ade;
        }


        .link {
            display: inline-block;
            font-size: 0.8em;
            width: 100%;
            text-decoration: none;
            color: #8a8a8ade;
            text-align: center;
            margin-top: 2%
        }

        .textH {

            margin-top: 5%;
            font-size: 100%;
            color: #8a8a8ade;
            text-align: center;
        }

        .link:hover {
            text-decoration: underline;
        }

        .btn {
            position: relative;
            left: 50%;
            bottom: 0;
            transform: translate(-50%, 50%);
            width: 120px;
            height: 45px;
            border-radius: 50px;
            background-color: #8a8a8ade;
            border: none;
            outline: none;
            color: #fff;
            cursor: pointer;
        }

        .tab-footer {
            position: absolute;
            bottom: 5%;
            left: 50%;
            transform: translate(-50%, 0);
            height: 30px;
            display: flex;
        }

        .tab-link {
            text-decoration: none;
            color: #8a8a8ade;
            margin: 0 3%;
        }

        .new {
            font-weight: bold;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            text-align: center;
            margin-top: 0px;

        }

        .lt {
            margin-top: 10px;
        }

        .exp {
            height: 15px;
        }

        .exe {
            height: 90px;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="textH">
            <b> Inscription </b>
        </div>

        <div class="exp"></div>
        <form method="POST" action="{{ route('inscription') }}">
            @csrf

            <!-- Matricule -->
            <div class="row">
                <i class="far fa-user top"></i>
                <input id="matricule" name="matricule" class="@error('matricule') is-invalid @enderror input"
                    value="{{ old('matricule') }}" placeholder="Matricule" autocomplete="matricule" autofocus required>
                @error('matricule')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <!-- Nom -->
            <div class="row">
                <i class="far fa-user top"></i>
                <input id="name" type="text" name="nom" class="@error('name') is-invalid @enderror input"
                    placeholder="Nom" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <!-- Prenom -->
            <div class="row">
                <i class="far fa-user top"></i>
                <input type="text" name="prenom" class="@error('name') is-invalid @enderror input"
                    placeholder="Prénoms" value="{{ old('prenom') }}" required autocomplete="prenom" autofocus>
                @error('prenom')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <!-- Filières -->
            <div class="row">
                <i class="fa-sharp fa-solid fa-person top"></i>
                <select id="filiere" class="@error('name') is-invalid @enderror select" name="filiere" required
                    autocomplete="filiere" autofocus>
                    <option selected> Filière </option>
                    @foreach ($fils as $fil)
                        <option value="{{ $fil->id }}">{{ $fil->nom }}
                        </option>
                    @endforeach
                </select>
                @error('filiere')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <!-- Email -->
            <div class="row">
                <i class="fa-regular fa-envelope top icon"></i>
                <input id="email" type="email" class="@error('email') is-invalid @enderror input"
                    placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <!-- Mot de passe -->
            <div class="row">
                <i class="fas fa-lock tel"></i>
                <input id="password" type="password" class="@error('password') is-invalid @enderror input"
                    placeholder="Mot de passe" name="password" required autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <!-- Confirmer mot de passe -->
            <div class="row">
                <i class="fas fa-lock tel"></i>
                <input id="password-confirm" type="password" class="input" placeholder="Confirmer mot de passe"
                    name="password_confirmation" required autocomplete="new-password">
            </div>

            <div class="exp"></div>
            <button class="new active btn" type="submit">
                <h3 class="lt"> S'inscrire </h3>
            </button>
            <br>
        </form>
    </div>
    <div class="exe"></div>

    <script>
        const numberInput = document.getElementById('matricule');
        numberInput.addEventListener('input', function(e) {
            const inputText = e.target.value;
            const numericValue = parseFloat(inputText);
            if (isNaN(numericValue)) {
                e.target.value = '';
            } else {
                e.target.value = numericValue;
            }
        });
    </script>
</body>

</html>
