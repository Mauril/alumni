<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script src="all.js"></script>
    <link rel="stylesheet" href="all.css">
    <title>Page de connexion</title>
    <style>
        @import url(./template1/css.css);

        * {
            box-sizing: border-box;
        }

        body {

            margin: 0;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-repeat: no-repeat;
            background: url(./template1/assets/images/portfolio-left-dec.png);
        }

        .container {
            position: relative;
            width: 450px;
            min-height: 300px;
            height: auto;
            max-height: 520px;
            background-color: #fffdfc;
            margin-top: 5%;
            padding: 15px 25px;
            border-radius: 10px;
            font-size: 18px;
            font-family: 'Quicksand', sans-serif;
        }



        .tab-body {
            position: relative;
            display: none;

            height: auto;
            max-height: 320px;
            padding: 15% 5%;
            margin-top: 20%;
            background-color: #fff;
            color: #82ade6de;
            box-shadow: 0px 5px 10px #7c8395;
            border-radius: 30px;
            font-size: 1rem;
        }

        .tab-body.active {
            display: block;
        }

        .top {
            color: #8a8a8ade;
        }


        .row {
            display: flex;
            justify-content: space-between;
            border-bottom: 1px solid #8a8a8ade;
            padding: 1.8% 0;
            margin-top: 20px;
        }

        .row .input {
            width: 90%;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        label {
            font-size: 0.9rem;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-size: 13px;
            margin-left: 20px;
        }

        span {
            font-size: 0.9rem;
            border: none;
            outline: none;
            background-color: transparent;
            color: #f78181de;
            font-weight: bold;
            margin-right: auto;
            font-size: 13px;
        }

        .tl {
            font-family: "Quicksand", sans-serif;
        }


        .row .date {
            width: 90%;
            border: none;
            outline: none;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        .row select {
            width: 90%;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        .input::placeholder {
            color: #8a8a8ade;
            font-family: 'Quicksand', sans-serif;
        }



        .select {
            margin-left: 7%;
        }

        .tel {
            color: #8a8a8ade;
        }

        .icon {
            color: #8a8a8ade;
        }


        .link {
            display: inline-block;
            font-size: 0.8em;
            width: 100%;
            text-decoration: none;
            color: #8a8a8ade;
            text-align: center;
            margin-top: 2%;
        }

        .textH {

            margin-top: 5%;
            font-size: 100%;
            color: #8a8a8ade;
            text-align: center;
        }

        .link:hover {
            text-decoration: underline;
        }

        .btn {
            position: relative;
            left: 50%;
            bottom: 0;
            transform: translate(-50%, 50%);
            width: 120px;
            height: 45px;
            border-radius: 50px;
            background-color: #8a8a8ade;
            border: none;
            outline: none;
            color: #fff;
            cursor: pointer;
        }

        .tab-footer {
            position: absolute;
            bottom: 5%;
            left: 50%;
            transform: translate(-50%, 0);
            height: 30px;
            display: flex;
        }

        .tab-link {
            text-decoration: none;
            color: #8a8a8ade;
            margin: 0 3%;
        }

        .new {
            font-weight: bold;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            text-align: center;
            margin-top: 0px;

        }

        .lt {
            margin-top: 10px;
        }

        .exp {
            height: 15px;
        }

        .exe {
            height: 90px;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="textH">
            <b> Connexion </b>
        </div>

        <div class="exp"></div>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="row">
                <i class="fa-regular fa-envelope top icon"></i>
                <input id="email" type="email" name="email" class="@error('email') is-invalid @enderror input"
                    placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>


            <div class="row">
                <i class="fas fa-lock tel"></i>
                <input id="password" type="password" name="password"
                    class="input @error('password') is-invalid @enderror" placeholder="Mot de passe" required
                    autocomplete="current-password">
            </div>

            <div class="exp"></div>
            <button class="new active btn" type="submit">
                <h3 class="lt"> Se connecter </h3>
            </button>


        </form>
        @if (Route::has('password.request'))
            <div class="text-center mt-3">
                <a class="link" href="{{ route('password.request') }}">
                    {{ __('Mot de passe oublié?') }}
                </a>
            </div>
        @endif
    </div>
    <div class="exe"></div>


</body>

</html>
