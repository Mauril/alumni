<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <form action="{{route('save')}}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="position-relative form-group"><label for="exampleFile" class="">image</label><input name="profil_img" id="exampleFile" type="file" class="form-control-file">

        <div class="position-relative form-group"><label for="exampleFile" class="">pdf</label><input name="pdf" id="exampleFile" type="file" class="form-control-file" accept=".pdf">
        <button type="submit"> submit</button>

    </form>
</body>
</html>