<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script src="{{ asset('all.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('all.css') }}">
    <title>Creation de profil</title>
    <style>
        @import url(./template1/css.css);

        * {
            box-sizing: border-box;
        }

        body {

            margin: 0;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-repeat: no-repeat;
            background: url({{asset('./template1/assets/images/portfolio-left-dec.png')}});
        }

        .container {
            position: relative;
            width: 500px;
            min-height: 900px;
            height: auto;
            max-height: 520px;
            background-color: #fffdfc;
            margin-top: 25%;
            padding: 15px 25px;
            border-radius: 10px;
            font-size: 18px;
            font-family: 'Quicksand', sans-serif;
        }

        .logo {
            position: absolute;
            top: 9%;
            left: 50%;
            transform: translate(-50%, 0);
            width: 55px;
            height: 55px;
            border-radius: 50%;
            box-shadow: 0 5px 5px #ccc;
            font-size: 1.8em;
            background-color: #8a8a8ade;
            color: #f0f6fb;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .tab-body {
            position: relative;
            display: none;

            height: auto;
            max-height: 320px;
            padding: 15% 5%;
            margin-top: 20%;
            background-color: #fff;
            color: #82ade6de;
            box-shadow: 0px 5px 10px #7c8395;
            border-radius: 30px;
            font-size: 1rem;
        }

        .tab-body.active {
            display: block;
        }

        .top {
            color: #8a8a8ade;
        }


        .row {
            display: flex;
            justify-content: space-between;
            border-bottom: 1px solid #8a8a8ade;
            padding: 1.8% 0;
            margin-top: 20px;
        }

        .row .input {
            width: 90%;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        label {
            font-size: 0.9rem;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-size: 13px;
            margin-left: 20px;
        }

        span {
            font-size: 0.9rem;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            margin-right: auto;
            font-size: 13px;
        }

        .error {
            color: #f78181de;
        }

        .tl {
            font-family: "Quicksand", sans-serif;
        }


        .row .date {
            width: 90%;
            border: none;
            outline: none;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        .row select {
            width: 90%;
            border: none;
            outline: none;
            background-color: transparent;
            color: #8a8a8ade;
            font-weight: bold;
            font-family: 'Quicksand', sans-serif;
        }

        .input::placeholder {
            color: #8a8a8ade;
            font-family: 'Quicksand', sans-serif;
        }



        .select {
            margin-left: 7%;
        }

        .tel {
            color: #8a8a8ade;
        }

        .icon {
            color: #8a8a8ade;
        }


        .link {
            display: inline-block;
            font-size: 0.8em;
            width: 100%;
            text-decoration: none;
            color: #8a8a8ade;
            text-align: right;
        }

        .textH {

            margin-top: 5%;
            font-size: 100%;
            color: #8a8a8ade;
            text-align: center;
        }

        .link:hover {
            text-decoration: underline;
        }

        .btn {
            position: relative;
            left: 50%;
            bottom: 0;
            transform: translate(-50%, 50%);
            width: 120px;
            height: 45px;
            border-radius: 50px;
            background-color: #8a8a8ade;
            border: none;
            outline: none;
            color: #fff;
            cursor: pointer;
        }

        .tab-footer {
            position: absolute;
            bottom: 5%;
            left: 50%;
            transform: translate(-50%, 0);
            height: 30px;
            display: flex;
        }

        .tab-link {
            text-decoration: none;
            color: #8a8a8ade;
            margin: 0 3%;
        }

        .new {
            font-weight: bold;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            text-align: center;
            margin-top: 0px;

        }

        .lt {
            margin-top: 10px;
        }

        .exp {
            height: 90px;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="textH">
            <b> Création de profil de l'alumni</b>
        </div>
        <div class="logo">
            <i class="fas fa-user "></i>
        </div>
        <div class="exp"></div>
        @if ($msg != null)
            <center>
                <span class="invalid-feedback error" role="alert">
                    <strong>{{ $msg }}</strong>
                </span>
            </center>
        @endif
        <form method="POST" action="{{ route('create_profil',['id'=>$id]) }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <i class="far fa-user top"></i>
                <input type="text" value="{{ $user->nom }}" name="nom" class="input" placeholder="Nom"
                    readonly>
            </div>

            <div class="row">
                <i class="far fa-user top"></i>
                <input type="text" value="{{ $user->prenom }}" name="prenoms" class="input" placeholder="Prénoms"
                    readonly>
            </div>
            <div class="row">
                <i class="far fa-user top"></i>
                <input id="age" type="text" name="age" class="input" placeholder="Age" required>
            </div>
            <div class="row">
                <i class="far fa-user top"></i>
                <input type="text" value="{{ $fil->nom }}" name="filiere" class="input" placeholder="Filière"
                    readonly>
            </div>
            <div class="row">
                <i class="far fa-user top"></i>
                <input type="text" name="soutenance" class="input" placeholder="Thème de soutenance" required>
            </div>

            <div class="row">
                <i class="fa-solid fa-building top"></i>
                <select class="select" name="entreprise">
                    <option selected>Entreprise actuelle</option>
                    @foreach ($entreprises as $ent)
                        <option value="{{ $ent->id }}">{{ $ent->nom }}
                        </option>
                    @endforeach
                </select>
                <a class="link" href="">Ajouter une entreprise</a>
            </div>
            <div class="row">
                <i class="fa-sharp fa-solid fa-person top"></i>
                <select class="select" name="competence">
                    <option selected>Compétences</option>
                    @foreach ($competences as $com)
                        <option value="{{ $com->id }}">{{ $com->nom }}
                        </option>
                    @endforeach
                </select>
                <a class="link" href="">Ajouter une compétence</a>
            </div>
            <div class="row">
                <i class="fa-solid fa-briefcase icon"></i>
                <select class="select" name="post">
                    <option selected>Poste actuelle</option>
                    @foreach ($posts as $post)
                        <option value="{{ $post->id }}">{{ $post->nom }}
                        </option>
                    @endforeach
                </select>
                <a class="link" href="">Ajouter un poste</a>
            </div>

            <div class="row">
                <i class="fa-regular fa-envelope top icon"></i>
                <input type="mail" value="{{ $user->email }}" name="email" class="input" placeholder="Email"
                    readonly>
            </div>
            <div class="row">
                <i class="fas fa-phone-square tel"></i>
                <input id="phone" name="telephone" class="input" placeholder="Téléphone" required>
            </div>

            <div class="row">
                <i class="fas fa-image top icon"></i>
                <label style="cursor:pointer; font-family:'Quicksand', sans-serif, " for="ipf">Choisissez une photo
                    de profil</label>
                <span>
                    <strong>&nbsp; : &nbsp;</strong>
                    <span id="file-name"> Aucun fichier sélectionné</span>
                </span>

                <input type="file" style="display: none;" id="ipf" title=" Veillez insérer votre photo"
                    name="profil_img" class="input" required>
            </div>

            <div class="row">
                <i class="fas fa-file top icon"></i>
                <label style="cursor:pointer; font-family:'Quicksand', sans-serif" for="ipf2">Insérer votre
                    CV</label>
                <span>
                    <strong>&nbsp; : &nbsp;</strong>
                    <span id="file-name2"> Aucun fichier sélectionné</span>
                </span>

                <input type="file" style="display: none;" id="ipf2" title=" Veillez insérer votre photo"
                    name="cv" class="input" required>
            </div>
            <input hidden value="{{ $user->id }}" name="id" type="text">
            <button class="new active btn" name="enrg" type="submit">
                <h3 class="lt"> Créer le profil </h3>
            </button>
        </form>
    </div>
    <div class="exp"></div>
    <script>
        let inputFile = document.getElementById("ipf");
        let fileName = document.getElementById("file-name");
        inputFile.addEventListener('change', function(event) {
            let uploadedFileName = event.target.files[0].name;
            fileName.textContent = uploadedFileName;
        })

        let inputFile2 = document.getElementById("ipf2");
        let fileName2 = document.getElementById("file-name2");
        inputFile2.addEventListener('change', function(event) {
            let uploadedFileName = event.target.files[0].name;
            fileName2.textContent = uploadedFileName;
        })

        const numberInput = document.getElementById('phone');
        numberInput.addEventListener('input', function(e) {
            const inputText = e.target.value;
            const numericValue = parseFloat(inputText);
            if (isNaN(numericValue)) {
                e.target.value = '';
            } else {
                e.target.value = numericValue;
            }
        });
        const numberInput2 = document.getElementById('age');
        numberInput2.addEventListener('input', function(e) {
            const inputText = e.target.value;
            const numericValue = parseFloat(inputText);
            if (isNaN(numericValue)) {
                e.target.value = '';
            } else {
                e.target.value = numericValue;
            }
        });
    </script>
</body>

</html>
