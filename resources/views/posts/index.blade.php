
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="all.css">
    <link rel="stylesheet" href={{asset("gi/posts/chat.css")}}>
    <link rel="stylesheet" href={{asset("gi/posts/boostrap.min.css")}}>
</head>


<style>
  .navbar ul {
    margin: 0;
    padding: 0;
    display: flex;
    list-style: none;
    align-items: center;
  }

  .navbar li {
    position: relative;
  }

  .navbar>ul>li {
    white-space: nowrap;
    padding: 10px 0 10px 30px;
  }

  .navbar a,
  .navbar a:focus {
    display: flex;
    align-items: center;
    justify-content: space-between;
    color: black;
    font-size: 16px;
    font-weight: 600;
    padding: 0;
    white-space: nowrap;
    transition: 0.3s;
    letter-spacing: 0.4px;
    position: relative;
  }
  .navbar a:hover{
    color: #03a4ed;
  }

  .list-group-item{

    border: solid rgb(27 172 240 / 26%) 2px !important ;
    padding: 15px;
    margin: 10px;
    border-radius: 10px;
  }

  .active{
    color: #03a4ed !important ;
  }
  .content{
    margin-top: 5% !important;
  }
</style>
<body class="">
  <!-- For demo purpose-->

    <header class="bg-light fixed-top shadow">
     <div class="container d-flex align-items-center justify-content-between ">
      <h1 class="logo">Alumni</h1>
      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link " href="{{route('profile.index')}}">Liste des alumnis</a></li>
          <li><a class="nav-link " href="{{route('userposts')}}">gerer mes publications</a></li>
          <li><a class="nav-link active" href="{{route('posts.index')}}">mur des publications</a></li>
          <li><a class="nav-link " href="{{route('home')}}">mon profil</a></li>
          <li>
            <form action="{{route('logout')}}" method="POST">
              @csrf
              <button type="submit" class="btn btn-sm btn-primary">Deconnexion</button>
            </form>
          </li>
        </ul>
      </nav>

     </div>
  </header>
  <div class="container content" >
  <div class="col rounded-lg overflow-hidden shadow" >
    <!-- Users box-->
      <div class="  ">

        <div class="bg-gray px-4 py-2 ">
          <h4 class=" mb-3 py-1" style="color: #ff695f">Publications récentes</h4>
          <hr width="50%">
        </div>

        <div class="messages-box">
          <div class="list-group " >
            @forelse($items as $item)
            <div class="list-group-item list-group-item-action mb-3" style="background-image: url({{asset('gi/posts/img/back.png')}}); background-repeat: no-repeat;"  >
              <div class="media">
                <img 
                            @if($item->user->profil_image !=null)
                            src = {{asset("storage/".$item->user->profil_image)}}
                            @else
                            src = {{asset("gi/posts/img/avatar.svg")}}
                            @endif
                            
                            alt="user" width="75" height = "75" style ="border-radius: 50%">
                <div class="media-body ml-4">
                  <div class="d-flex align-items-center justify-content-between mb-1">
                    <h6 class="m-2"> <a href="{{route('profile.show',['id'=>$item->user->id])}}">{{$item->user->nom}}</a> </h6><small class="small font-weight-bold"> publié le {{$item->date}}</small>
                  </div>
                  <p class=" m-2 ">
                    {{$item->contenu}}
                  </p>
                  @if($item->filieres()->exists())
                  <div class="text-muted">
                    <small ><b>filieres concernées  : </b>
                  {{ $item->filieres()->pluck('nom')->implode(', ')}}
                    </small>
                  </div>
                @endif
                @if(Auth::user()->role == "admin" && $item->destinataire == "ecole")
                  <div class="text-muted">
                    <small style="color: #03a4ed"><b>Pour l'administration</b></small>
                  </div>
                @endif
                </div>
                
              </div>
            </div>
            
            @empty
                <div class="h5">
                  Aucune publication
                </div>
            @endforelse

          </div>
        </div>
      </div>
    <!-- Chat Box-->
   
  </div>
</div>
<footer style="background-color: rgb(252, 243, 232); font-family: Verdana, Geneva, Tahoma, sans-serif;" class="text-center py-5 mt-5">
  <div class="row">
      
  </div>.
</footer>
</body>
</html>




