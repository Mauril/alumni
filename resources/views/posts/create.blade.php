<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href={{asset("gi/posts/boostrap.min.css")}}>
</head>


<style>

.navbar ul {
  margin: 0;
  padding: 0;
  display: flex;
  list-style: none;
  align-items: center;
}

.navbar li {
  position: relative;
}

.navbar>ul>li {
  white-space: nowrap;
  padding: 10px 0 10px 30px;
}

.navbar a,
.navbar a:focus {
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: black;
  font-size: 16px;
  font-weight: 600;
  padding: 0;
  white-space: nowrap;
  transition: 0.3s;
  letter-spacing: 0.4px;
  position: relative;
  
}
.navbar a:hover{
  color: #03a4ed;
}
.content{
    margin-top: 5% !important;
  }

    
</style>
<body class="">
  <!-- For demo purpose-->

    <header class="bg-light fixed-top shadow">
     <div class="container d-flex align-items-center justify-content-between ">
      <h1 class="logo">Alumni</h1>

    <nav id="navbar" class="navbar">
      <ul>
        <li><a class="nav-link " href="{{route('profile.index')}}">Liste des alumnis</a></li>
        <li><a class="nav-link " href="{{route('userposts')}}">gerer mes publications</a></li>
        <li><a class="nav-link " href="{{route('posts.index')}}">mur des publications</a></li>
        <li><a class="nav-link " href="{{route('home')}}">mon profil</a></li>
        
        <li>
          <form action="{{route('logout')}}" method="POST">
            @csrf
            <button type="submit" class="btn btn-sm btn-primary">Deconnexion</button>
          </form>
        </li>
      </ul>
    </nav>

     </div>
  </header>
  <div class="container content" >
  <div class="col rounded-lg overflow-hidden shadow" >
    <!-- Users box-->
      <div class="  ">

        <div class="bg-gray px-4 py-2 ">
          <h4 class=" mb-3 py-1 text-center" style="color: #ff695f">Creation de publication</h4>
          <hr width="50%">
        </div>

        
          <div class="main-card mb-3 card">
              <div class="card-body">
                  <form class="" action="{{route('posts.store')}}" method="POST">
                    @csrf
                    <div class="position-relative form-group"><label for="exampleText" class=""><b>Entrez le message </b></label><textarea name="contenu" id="exampleText" class="form-control" required></textarea></div>
                    <div class="position-relative form-group"><label for="exampleText" class=""> <b>Selectionner les destinataires de la publication</b> </label>
                      <select class="form-control-sm form-control" name="destinataire" id="destinataires">
                        <option value="all">tout le monde</option>
                        @if(Auth::user()->role != "admin")
                        <option value="ecole">Administration uniquement</option>
                        @endif
                        <option value="filieres">alumnis d'une ou plusieurs filieres donnée</option>
                    </select>

                    <div class="position-relative form-group mt-4" id="filiereCheckbox" style="display:none"><label>Selectionner les filieres concernées</label>
                      @foreach($items as $item)
                      <div class="position-relative form-check">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name ={{$item->nom}}>{{$item->nom}}</label>
                      </div>
                      @endforeach
                    </div>
                    @if($errors->any())
                    @foreach($errors->all() as $error)
                          <small class="form-text text-muted">{{$error}}</small>
                    @endforeach
                    @endif
                      </div>

                     
                      <button class="mt-4 btn btn-success">Créer la publication</button>
                  </form>
              </div>
          </div>
         
          </div>
        </div>
      </div>
    <!-- Chat Box-->
   
  </div>
</div>
<footer style="background-color: rgb(252, 243, 232); font-family: Verdana, Geneva, Tahoma, sans-serif;" class="text-center py-5 mt-5">
  <div class="row">
      
  </div>
</footer>
<script>
          const destinataire = document.getElementById("destinataires");
          const filiereCheckbox = document.getElementById("filiereCheckbox");

          destinataire.addEventListener("change",function(){
             destinataire.value == "filieres" ? filiereCheckbox.style.display ="block" : filiereCheckbox.style.display = "none"

            }
          )
       

</script>

</body>
</html>




