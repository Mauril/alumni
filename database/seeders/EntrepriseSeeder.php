<?php

namespace Database\Seeders;


use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EntrepriseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $localisations = ['Cotonou', 'Porto-Novo', 'Abomey-Calavi', 'Parakou', 'Bohicon'];
        $faker = Faker::create();

        for ($i = 0; $i < 6; $i++) {
            DB::table('entreprises')->insert([
                'nom' => $faker->company(),
                'localisation' => $localisations[array_rand($localisations)]
            ]);
        }
    }
}
