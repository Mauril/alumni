<?php

namespace Database\Seeders;

use App\Models\Publication;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            CompetenceSeeder::class,
            EntrepriseSeeder::class,
            PostSeeder::class,
            DepartementSeeder::class,
            FiliereSeeder::class,
            UserSeeder::class,
            PublicationSeeder::class
        ]);
    }
}
