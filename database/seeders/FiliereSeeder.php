<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class FiliereSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('filieres')->insert([
            [
                'nom' => 'IG',
                'departement_id' => 1
            ],
            [
                'nom' => 'STAT',
                'departement_id' => 3
            ],
            [
                'nom' => 'PLAN',
                'departement_id' => 3
            ],
            [
                'nom' => 'GBA',
                'departement_id' => 2
            ],
            [
                'nom' => 'GC',
                'departement_id' => 2
            ],
            [
                'nom' => 'GTL',
                'departement_id' => 2
            ]
        ]);
    }
}
