<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class PublicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $destinataires = ['all','ecole'];
        $faker = Faker::create();
        for($i=0;$i<10;$i++){
            DB::table('publications')->insert([
                'contenu'=>$faker->text($maxNbChars = 200),
                'date'=>$faker->date(),
                'destinataire'=>$destinataires[array_rand($destinataires)],
                'user_id'=>$faker->numberBetween(1,6)
            ]);
        }
    }
}
