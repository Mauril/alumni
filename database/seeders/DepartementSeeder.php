<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DepartementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departements')->insert(
            [
                [
                    'nom' => 'Informatique'
                ],
                [
                    'nom' => 'Management'
                ],
                [
                    'nom' => 'Planification'
                ]
            ]
        );
    }
}
