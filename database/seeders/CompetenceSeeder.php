<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CompetenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('competences')->insert([
            [
                'nom'=>'Administrateur reseau'
            ],
            [
                'nom'=>'Developpeur'
            ],
            [
                'nom'=>'Chef de projet'
            ],
            [
                'nom'=>'graphiste'
            ]
            ]
            );
    }
}
