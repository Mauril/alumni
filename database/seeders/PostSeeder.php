<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = ['Directeur', 'Secretaire', 'Comptable', 'Coodornateur'];
        for ($i = 0; $i < 4; $i++) {
            DB::table('posts')->insert([
                'nom' => $posts[$i]
            ]);
        }
    }
}
