<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('fr_FR');


        DB::table('users')->insert([

            [
                'nom' => 'ASSOGBA',
                'prenom' => 'Robert',
                'matricule' => 196322,
                'filiere_id' => 1,
                'annee_fin_etude' => $faker->date(),
                'inscrit' => false,
                'role' => 'alumni'
            ],
            [
                'nom' => 'AKAN',
                'prenom' => 'Silva',
                'matricule' => 121213,
                'filiere_id' => 3,
                'annee_fin_etude' => $faker->date(),
                'inscrit' => false,
                'role' => 'alumni'
            ],
            [
                'nom' => 'DEGAN',
                'prenom' => 'Richard',
                'matricule' => 163254,
                'filiere_id' => 2,
                'annee_fin_etude' => $faker->date(),
                'inscrit' => false,
                'role' => 'alumni'
            ],
            [
                'nom' => 'TOVI',
                'prenom' => 'Paul',
                'matricule' => 145233,
                'filiere_id' => 5,
                'annee_fin_etude' => $faker->date(),
                'inscrit' => false,
                'role' => 'alumni'
            ],
            [
                'nom' => 'AYILE',
                'prenom' => 'Amen',
                'matricule' => 223113,
                'filiere_id' => 6,
                'annee_fin_etude' => $faker->date(),
                'inscrit' => false,
                'role' => 'alumni'
            ],
            [
                'nom' => 'BOKO',
                'prenom' => 'Julien',
                'matricule' => 123336,
                'filiere_id' => 4,
                'annee_fin_etude' => $faker->date(),
                'inscrit' => false,
                'role' => 'alumni'
            ]

        ]);
    }
}
