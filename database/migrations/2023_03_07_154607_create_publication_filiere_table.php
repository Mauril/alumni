<?php

use App\Models\Filiere;
use App\Models\Publication;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filiere_publication', function (Blueprint $table) {
            $table->primary(['publication_id', 'filiere_id']);
            $table->foreignIdFor(Publication::class);
            $table->foreignIdFor(Filiere::class);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publication_filiere');
    }
};
