<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->integer('matricule')->nullable();
            $table->string('username')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable()->unique();
            $table->integer('age')->nullable();
            $table->date('annee_fin_etude')->nullable();
            $table->string('theme_soutenance')->nullable();
            $table->string('cv')->nullable();
            $table->string('telephone')->nullable();
            $table->string('profil_image')->nullable();
            $table->boolean('inscrit')->nullable();
            $table->string('role')->nullable();
            $table->boolean('profile_complete')->default(false);

            //foreign keys
            $table->foreignId('competence_id')->nullable()->constrained()->onDelete('restrict')->onUpdate('restrict');
            $table->foreignId('entreprise_id')->nullable()->constrained()->onDelete('restrict')->onUpdate('restrict');
            $table->foreignId('post_id')->nullable()->constrained()->onDelete('restrict')->onUpdate('restrict');
            $table->foreignId('filiere_id')->nullable()->constrained()->onDelete('restrict')->onUpdate('restrict');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
