<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AcceuilController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\InscriptionController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\PublicationController;
use App\Models\Publication;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth', 'profil_complete:1'])->group(function () {
    Route::get('mon_profile', [ProfilController::class, 'showmyprofile'])->name('home');
});

Route::view('/', 'acceuil')->name('acceuil');
Route::post('/register', [InscriptionController::class, 'register'])->name('inscription');
Route::get('/creation-de-profil/{id}', [ProfilController::class, 'page_profil'])->name('page_profil');
Route::post('/creation-de-profil/{id}', [ProfilController::class, 'create_profil'])->name('create_profil');
Route::resource('posts',PublicationController::class);
Route::get('user-posts/',[PublicationController::class,'userposts'])->name('userposts');
Route::get('profile/{id}',[ProfilController::class,'show'])->name('profile.show');
// Route::get('mon_profile',[ProfilController::class,'showmyprofile'])->name('profile.show.me');
Route::get('alumnis',[ProfilController::class,'index'])->name('profile.index');
Route::get('save',[Controller::class,'form'])->name('form');
Route::post('save',[Controller::class,'save'])->name('save');
Route::get('modify_profil',[ProfilController::class,'modify_page'])->name('profile.modify');
Route::post('modify_profil',[ProfilController::class,'modify_profil'])->name('profile.update');