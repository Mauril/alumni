<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function form(){

        return view('save');
    }

    public function save(){
        $imgname=time().'.'.request('profil_img')->extension();
        $imgpath=request()->file('profil_img')->storeAs(
              'Avatars',
              $imgname,
              'public'
          );
          $pdfname=time().'.'.request('pdf')->extension();
        $pdfpath=request()->file('pdf')->storeAs(
              'Avatars',
              $pdfname,
              'public'
          );

          dd([$pdfpath,$imgpath]);
        
    }
}
