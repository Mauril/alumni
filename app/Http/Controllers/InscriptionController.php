<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Filiere;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class InscriptionController extends Controller
{

    public function register_page()
    {
        $fils = Filiere::all();
        return view('auth.register', compact('fils'));
    }
    public function register(Request $request)
    {
        $request->validate([
            'nom' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed', 'unique:users'],
        ]);

        $v = User::where('matricule', $request->matricule)
            ->where('nom', $request->nom)
            ->where('prenom', $request->prenom)
            ->where('filiere_id', $request->filiere)->first();

        if ($v == null) {
            abort(404);
        } else {

            $id = $v->id;
            $user = User::findOrFail($id);
            if ($v->email == null and $v->password == null) {
                $user->update([
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'inscrit' => true
                ]);
                $id = encrypt($id);

                //connection of user after inscription

                return redirect()->route('page_profil',['id'=>$id]);
            } else {
                return redirect()->route('login')->with('message', 'Vous possedez déjà un compte . Veillez vous connectez !');
            }
        }
    }
}
