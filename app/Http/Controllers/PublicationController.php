<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Filiere;
use App\Models\Publication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
       
        if($role == "admin"){
            $posts = Publication::orderBy('date','DESC')->get();
        }else{
            $fils = [Auth::user()->filiere->nom];
            $posts=Publication::where('destinataire','all')->orWhere('destinataire','filieres')->whereHas('filieres',
            function ($query) use ($fils){
                $query->whereIn('nom',$fils);
            }
            )->orderBy('date','DESC')->get();
        }
        foreach($posts as $post){
            $post->date = \Carbon\Carbon::parse($post->date)->locale('fr')->isoFormat('D MMMM YYYY');
        }
        return view('posts.index',['items'=>$posts]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $filieres = Filiere::all();
        return view('posts.create',['items'=>$filieres]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'contenu'=> ['required','min:10'],
            'destinataire'=>['required']
        ]);
        
        $filieres = Filiere::all();
        $pub = Publication::create([
            'contenu' =>$request->contenu,
            'date'=>now(),
            'destinataire'=>$request->destinataire,
            'user_id'=>Auth::user()->id
        ]);
        if($request->destinataire == "filieres"){
           foreach($filieres as $filiere){
            if($request[$filiere->nom] == "on"){
                $pub->filieres()->attach($filiere->id);
            }
           }
        }

        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Publication::find($id);
        if(Auth::user()->role == "admin" || $post->user_id == Auth::user()->id){
            $post->delete();
            return redirect(route('userposts'));
        }else{
            abort(403);
        }
        
    }
    public function userposts(){

        if(Auth::user()->role == "admin"){
            $posts = Publication::orderBy('date','DESC')->get();
        }else{
            $id = Auth::user()->id;
            $posts = Publication::where('user_id',$id)->orderBy('date','DESC')->get();
        }


        return view('posts.userposts',['items'=>$posts]);
    }
}
