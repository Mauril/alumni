<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Models\Filiere;
use App\Models\Competence;
use App\Models\Entreprise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Encryption\DecryptException;

class ProfilController extends Controller
{
    public function index(){
        $users = User::where('role','alumni')->get();
        return view('profile.index',['items'=>$users]);
    }
    public function page_profil($id)
    {
        $competences = Competence::all();
        $posts = Post::all();
        $entreprises = Entreprise::all();
        try{
            $id = decrypt($id);
        }catch(DecryptException){
            abort(404);
        }
       
        $msg = session('message');
        $user = User::findOrFail($id);
        $fil = Filiere::findOrFail($user->filiere_id);
        $id = encrypt($id);
        return view('create-profil', compact('id','user', 'msg', 'competences', 'posts', 'entreprises', 'fil'));
    }

    public function create_profil(Request $request,$id)
    {
        $imgname = time() . '.' . request('profil_img')->extension();
        $imgpath = request()->file('profil_img')->storeAs(
            'Avatars',
            $imgname,
            'public'
        );

        $cvname = time() . '.' . request('cv')->extension();
        $cvpath = request()->file('cv')->storeAs(
            'Avatars',
            $cvname,
            'public'
        );

        if (Auth::user()) {

            $id = Auth::user()->id;
            $user = User::findOrFail($id);
            $user->update([
                'age' => $request->age,
                'theme_soutenance' => $request->soutenance,
                'entreprise_id' => $request->entreprise,
                'competence_id' => $request->competence,
                'post_id' => $request->post,
                'telephone' => $request->telephone,
                'profil_image' => $imgpath,
                'cv' => $cvpath,
                'profile_complete' => true
            ]);
            $user->save();
            return redirect(route('home'));
        } else {
            try{
                $id0 = decrypt($id);
            }catch(DecryptException){
                abort(404);
            }
            $user0 = User::findOrFail($id0);
            Auth::login($user0);
            $user0->update([
                'age' => $request->age,
                'theme_soutenance' => $request->soutenance,
                'entreprise_id' => $request->entreprise,
                'competence_id' => $request->competence,
                'post_id' => $request->post,
                'telephone' => $request->telephone,
                'profil_image' => $imgpath,
                'cv' => $cvpath,
                'profile_complete' => true
            ]);
            $user0->save();
            return redirect(route('home'));
        }
    }

    public function modify_page()
    {

        $id = Auth::user()->id;
        $competences = Competence::all();
        $posts = Post::all();
        $entreprises = Entreprise::all();
        $user = User::findOrFail($id);
        return view('modify-profil', compact('user','competences', 'posts', 'entreprises'));
    }

    public function modify_profil(Request $request)
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
       
        if($request->profil_img !=null){
            $imgname = time() . '.' . request('profil_img')->extension();
            $imgpath = request()->file('profil_img')->storeAs(
                'Avatars',
                $imgname,
                'public'
            );
            $user->update(['profil_image' => $imgpath]);
            $user->save();
        }
        if($request->cv !=null){
            $cvname = time() . '.' . request('cv')->extension();
            $cvpath = request()->file('cv')->storeAs(
                'Avatars',
                $cvname,
                'public'
            );
            $user->update(['cv' => $cvpath]);
            $user->save();
        }
         $user->update([
                'age' => $request->age,
                'theme_soutenance' => $request->soutenance,
                'entreprise_id' => $request->entreprise,
                'competence_id' => $request->competence,
                'post_id' => $request->post,
                'telephone' => $request->telephone,
            ]);
            $user->save();
            return redirect(route('home'));
            
        }
    
    public function show($id){

        $user = User::findOrFail($id);
        return view('profile.show',['item'=>$user]);
    }

    public function showmyprofile(){
        if(!Auth::user()){
            return redirect(route('login'));
        }else{
            $user = Auth::user();
            return view('myprofil',['item'=>$user]);
        }
    }
}
