<?php

namespace App\Http\Middleware;

use Closure;
use Hamcrest\Type\IsBoolean;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Alumiconnect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, int $profil_complete)
    {
        if(Auth::user() == null){
            return redirect(route('login'));
        }
        if ($request->user()->profile_complete == $profil_complete) {
            if($request->user()->role == "admin"){
                return redirect(route('profile.index'));
            }else{
                return $next($request);
            }
        } else {
            if (Auth::user()) {
                $id = encrypt(Auth::user()->id);
                $message = 'Vous devez préalablement créé votre profil';
                return redirect()->route('page_profil',['id'=>$id])
                    ->with('message', $message);
            } else {
                abort(403);
            }
        }
    }
}
