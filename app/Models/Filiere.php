<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $departement_id
 * @property string $nom
 * @property Departement $departement
 * @property User[] $users
 */
class Filiere extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['departement_id', 'nom'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function departement()
    {
        return $this->belongsTo('App\Models\Departement');
    }

    public function publications()
    {
        return $this->belongsToMany('App\Models\Publication');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
