<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $contenu
 * @property string $date
 * @property string $destinataire
 * @property User $user
 */
class Publication extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'contenu', 'date', 'destinataire'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function filieres()
    {
        return $this->belongsToMany('App\Models\Filiere');
    }
}
