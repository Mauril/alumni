<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nom
 * @property string $localisation
 * @property User[] $users
 */
class Entreprise extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['nom', 'localisation'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
