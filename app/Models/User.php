<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property integer $id
 * @property integer $competence_id
 * @property integer $entreprise_id
 * @property integer $post_id
 * @property integer $filiere_id
 * @property string $nom
 * @property string $prenom
 * @property integer $matricule
 * @property string $username
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property integer $age
 * @property string $annee_fin_etude
 * @property string $theme_soutenance
 * @property string $cv
 * @property string $telephone
 * @property string $profil_image
 * @property boolean $inscrit
 * @property string $role
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Publication[] $publications
 * @property Post $post
 * @property Competence $competence
 * @property Filiere $filiere
 * @property Entreprise $entreprise
 */
class User extends Authenticatable
{
    /**
     * @var array
     */
    protected $fillable = ['competence_id', 'entreprise_id', 'post_id', 'filiere_id', 'nom', 'prenom', 'matricule', 'username', 'email', 'email_verified_at', 'password', 'age', 'annee_fin_etude', 'theme_soutenance', 'cv', 'telephone', 'profil_image', 'inscrit', 'role','profile_complete', 'remember_token', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function publications()
    {
        return $this->hasMany('App\Models\Publication');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function competence()
    {
        return $this->belongsTo('App\Models\Competence');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function filiere()
    {
        return $this->belongsTo('App\Models\Filiere');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entreprise()
    {
        return $this->belongsTo('App\Models\Entreprise');
    }
}
